#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Frederico Sales
<frederico.sales@engenharia.ufjf.br>
2018
"""

# import
import numpy as np 
import matplotlib.pyplot as plt 
import math
import seaborn as sns


# class
class Dummy(object):
	"""Dummy class."""

	def __init__(self, data, alpha=0.90):
		"""
		Construtor
		:param data: vetor de valores
		:param alpha: Z(alpha/2)
		"""
		self.data = data
		self.alpha = alpha

	def media(self):
		"""
		Calcula a média de uma lista
		"""
		media = sum(self.data) / len(self.data)
		return media

	def variancia(self):
		"""Calcula a variancia"""
		var = 0
		for i in range(len(self.data)):
			# var += (self.data[i] - self.media())**2
			var += math.pow((self.data[i] - self.media()), 2)
		var = var / float(len(self.data) - 1)
		return var

	def desvio_padrao(self):
		"""Calcula o desvio padrao."""
		return math.sqrt(self.variancia())

	def minimo(self):
		"""Retorna o menor valor do vetor."""
		return np.min(self.data)

	def maximo(self):
		"""Retorna o maior valor do vertor."""
		return np.max(self.data)

	def somatorio(self):
		"""Retorna a soma dos valores de um vetor."""
		return sum(self.data)

	def intervalo_confianca(self):
		"""Retorna o li e ls do intervalo de confianca."""
		li = 0
		ls = 0
		li = self.media() - self.alpha * (self.desvio_padrao() / math.sqrt(len(self.data)))
		ls = self.media() + self.alpha * (self.desvio_padrao() / math.sqrt(len(self.data)))
		return li, ls

	def cdf(self, xlabel=None, ylabel=None):
		"""
		Plota a CDF
		:param xlabel: xlabel
		:param ylabel: ylabel
		"""

		# tamanho do vetor
		data_size = len(self.data)

		# seta os bins
		data_set = sorted(set(self.data))
		bins = np.append(data_set, data_set[-1] + 1)

		# usa o histograma para setar os bins
		counts, bin_edges = np.histogram(self.data, bins=bins, density=False)
		counts = counts.astype(float) / data_size

		#
		cdf = np.cumsum(counts)

		# plot
		sns.set()
		plt.plot(bin_edges[0:-1], cdf, linestyle='--', marker='o', color='b')
		plt.xlabel(xlabel)
		plt.ylim(0, 1)
		plt.ylabel(ylabel)
		plt.savefig('img/cdf.png')
		plt.show()
