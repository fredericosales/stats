#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Frederico Sales
<frederico.sales@engenharia.ufjf.br>
2018
"""

# imports
from __init__ import *


if __name__ == "__main__":
    # do some:
    data = np.array([1,1,2,3,3,4,5,6,6,6,7,8,9,9,9,9])
    user  = Dummy(data, 0.95)
    print("{:.4f}".format(user.media()))
    print("{:.4f}".format(user.variancia()))
    print("{:.4f}".format(user.desvio_padrao()))
    print("{:.4f}".format(user.minimo()))
    print("{:.4f}".format(user.maximo()))
    print("{:.4f}".format(user.somatorio()))
    print("{:.4f}".format(user.intervalo_confianca()[0]))
    print("{:.4f}".format(user.intervalo_confianca()[1]))
    user.cdf('dados', 'CDF')